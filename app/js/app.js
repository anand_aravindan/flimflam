/*
Updates the catalog section based on the search.
 */
var Catalog = (function(){
	return{
		show:function(item){
			var dataService = DataService.getInstance();
			var container = $("#products-container");
			container.empty();

			/*Templates that provides ellipses if the text is very large*/
			var ellipseTemplate = $("#ellipse-template")[0].innerHTML;

			/*Product template containing the image, itemcode, description and price*/
			var template  = $("#product-template");

			dataService.getItems(item).then(function(items){
				if(items && items.length>0){
					items.forEach(function(item){
						/*clone the template for every product to be displayed*/
						var cloned = template.clone(true);

						/*find the data nodes using the attribute in the template and update
						 with the corresponding property of the item
						 */
						cloned.find("[data-content]").each(function(index,element){
							var data = item[element.getAttribute("data-content")];
							if(element instanceof HTMLImageElement){
								element.src = data;
							}else{
								var html = "";
								html = html + data;
								if(element.hasAttribute("data-ellipse")){
									element.title = data;
									html = html + ellipseTemplate;
								}
								element.innerHTML = html;
							}

						});
						container.append(cloned);
					});
				}else{
					/*
					Showing no products found if not found.
					 */
					var text = 'No products found';
					if(item && item.length > 0){
						text = text + " for '"+item+"'";
					}
					container.append('<h3>'+text+'</h3>');
				}
			})
		}
	}
})();

Catalog.show();