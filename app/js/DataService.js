/*
Configuration Detail.
All urls and convenience methods for getting the urls.
 */
var BoarsConfig = (function () {
    function BoarsConfig() {
    }
    BoarsConfig.addKey = function (url) {
        return url + "?key=" + this.API_KEY;
    };
    BoarsConfig.GET_ITEMS_RESOURCE = function () {
        return this.addKey(this.GET_ITEMS_URL);
    };
    BoarsConfig.GET_ITEMS_BYID_RESOURCE = function (item) {
        return this.addKey(this.GET_ITEM_BY_ID_URL + "" + item);
    };
    BoarsConfig.GET_CATEGORY_RESOURCE = function () {
        return this.addKey(this.GET_CATEGORIES_URL);
    };
    BoarsConfig.GET_ITEM_IMAGES_RESOURCE = function (item, type) {
        var resource = this.GET_ITEM_IMAGES_URL + "" + item;
        if (type) {
            resource + "@" + type;
        }
        return resource + ".jpg";
    };
    BoarsConfig.API_KEY = "pfB5qzSDzmvxc2A16e05";
    BoarsConfig.GET_ITEMS_URL = "http://api.flim-flamming.com/items";
    BoarsConfig.GET_ITEM_BY_ID_URL = "http://api.flim-flamming.com/items/";
    BoarsConfig.GET_CATEGORIES_URL = "http://api.flim-flamming.com/categories";
    BoarsConfig.GET_ITEM_IMAGES_URL = "http://api.flim-flamming.com/assets/images/";
    return BoarsConfig;
})();
/*
Service used to interact with the server.  Singleton, use getInstance method.
 productsMeta - holds all product meta(only itemcode and description)
 products - Holds all product details including the image url.
 */
var DataService = (function () {
    function DataService() {
        if (DataService._instance) {
            throw new Error("Error: Instantiation failed: Use DataService.getInstance() instead of new.");
        }
    }
    DataService.getInstance = function () {
        if (DataService._instance == null) {
            DataService._instance = new DataService();
        }
        return DataService._instance;
    };
    /*
    Provides all products metadata after caching.
     */
    DataService.prototype.getAllItemsMeta = function () {
        var deferred = $.Deferred();
        if (DataService.productsMeta && DataService.productsMeta.length > 0) {
            setTimeout(function () {
                deferred.resolve(DataService.productsMeta);
            }, 0);
            return deferred;
        }
        var resourceUrl = BoarsConfig.GET_ITEMS_RESOURCE();
        var jqxhr = $.ajax({
            url: resourceUrl
        }).done(function (data) {
            DataService.productsMeta = data;
            deferred.resolve(DataService.productsMeta);
        });
        return deferred;
    };
    /*
        Returns the Prodcut details promise for the given metadata
     */
    DataService.prototype.getItemPromise = function (productMeta) {
        var deferred = $.Deferred();
        var resourceUrl = BoarsConfig.GET_ITEMS_BYID_RESOURCE(productMeta.itemCode);
        $.ajax({ url: resourceUrl }).then(function (successResult) {
            deferred.resolve(successResult);
        }, function (failedResult) {
            deferred.resolve(null);
        });
        return deferred;
    };
    /*
     Combines array of promise into one promise.
     */
    DataService.prototype.all = function (arrayOfPromises) {
        return $.when.apply($, arrayOfPromises).then(function () {
            return Array.prototype.slice.call(arguments, 0);
        }, function () {
            console.error("Promise failed when getting items array.");
            return Array.prototype.slice.call(arguments, 0);
        });
    };
    /*
        Provides all product details. Uses cache whenever available.
    */
    DataService.prototype.getAllItems = function () {
        var deferred = $.Deferred();
        if (DataService.products && DataService.products.length > 0) {
            setTimeout(function () {
                deferred.resolve(DataService.products);
            }, 0);
            return deferred;
        }
        else {
            var arrayOfItemPromise = [];
            ;
            var promiseToArrayOfProducts;
            var _this = this;
            this.getAllItemsMeta().then(function (productsList) {
                arrayOfItemPromise = $.map(productsList.products, _this.getItemPromise);
                promiseToArrayOfProducts = _this.all(arrayOfItemPromise);
                promiseToArrayOfProducts.then(function (products) {
                    products.forEach(function (product, index) {
                        if (product == null) {
                            //remove product from the list
                            products.splice(index, 1);
                            return;
                        }
                        product.imageUrl = BoarsConfig.GET_ITEM_IMAGES_RESOURCE(product.itemCode, "3x");
                    });
                    DataService.products = products;
                    deferred.resolve(DataService.products);
                });
            });
            return deferred;
        }
    };
    DataService.prototype.isProductMatched = function (product, searchKey) {
        var searchKey = searchKey.toUpperCase();
        return (product.itemCode.toUpperCase().indexOf(searchKey) != -1 || product.description.toUpperCase().indexOf(searchKey) != -1 || product.category.toUpperCase().indexOf(searchKey) != -1 || product.productName.toUpperCase().indexOf(searchKey) != -1);
    };
    DataService.prototype.getItems = function (searchKey) {
        var deferred = $.Deferred();
        var itemsToReturn = [];
        var _this = this;
        this.getAllItems().then(function (allItems) {
            if (searchKey && searchKey.length > 1) {
                allItems.forEach(function (product) {
                    if (_this.isProductMatched(product, searchKey)) {
                        itemsToReturn.push(product);
                    }
                });
                deferred.resolve(itemsToReturn);
            }
            else {
                setTimeout(function () {
                    deferred.resolve(allItems);
                }, 0);
            }
        });
        return deferred;
    };
    return DataService;
})();
