var gulp = require('gulp');
var usemin = require('gulp-usemin');
var uglify = require('gulp-uglify');
var minifyHtml = require('gulp-minify-html');
var minifyCss = require('gulp-minify-css');
var rev = require('gulp-rev');
var clean = require('gulp-clean');
var runSequence = require('gulp-run-sequence');
var serve = require('gulp-serve');
 
gulp.task('clean', function () {
    return gulp.src('build/*')
        .pipe(clean({force: true}))
});

gulp.task('usemin', function () {
  return gulp.src('app/index.html')
      .pipe(usemin({
        css: [minifyCss(), 'concat'],
        html: [minifyHtml({empty: true})],
        js: [uglify(), rev()],
        jslibs:[uglify(), rev()]
      }))
      .pipe(gulp.dest('build'));
});

gulp.task('copy-fonts',function(){
    return gulp.src('app/css/fonts/*')
        .pipe(gulp.dest('build/css/fonts'));
});

gulp.task('default',function(){
	runSequence('clean','copy-fonts','usemin','serve-prod');
});

gulp.task('dev', serve({
  port: 9998
}));

gulp.task('serve-prod', serve({
  root: ['build'],
  port: 8080
}));
